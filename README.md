# Ciência de Dados

## Projeto: titanic


## Ambiente Virtual

``` bash
# 1. Criando o ambiente virtual
python3 -m venv venv

# 2. Carregando o ambiente virtual
source venv/bin/activate

# 3.0 Requisitos 
(venv) pip freeze

    # 3.1 Salvando requisitos
(venv) pip freeze > requirements.txt

    # 3.2 Instalando requisitos
(venv) pip install -r requirements.txt

# 4. Instalando Jupyter Notebook
(venv) pip install jupyter


# 5. Adicionando o ambiente virtual à lista de kernels
(venv) python3 -m ipykernel install --user --name=venv

    # 5.1 Excluindo o ambiente virtual da lista de kernels
(venv) jupyter-kernelspec uninstall venv_ftc

# 6. Executando Jupyter notebook
(venv) jupyter notebook
```

